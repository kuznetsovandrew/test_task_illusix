# Тестовое задание для компании Illusix

<p>После клонирования репозитория скопировать файл .env.example в .env и указать подключение к ДБ</p>

### Запустить команды

- php artisan key:generate 
- php artisan migrate --seed

### Есть стартовый юзер с доступами: <br>
admin@gmail.com <br>
12345678

<p>Так же можно самостоятельно создать пользователя</p>

/register - урл для регистрации <br>
/login    - урл для авторизации

# Краткое описание
- Использован Laravel 5.8
<p>Кроме стартовых пакетов фреймверка были использованы следующие:</p>

- barryvdh/laravel-debugbar (debug-panel)
- barryvdh/laravel-ide-helper (генерирует мета информацию для IDE)
- jeroennoten/laravel-adminlte (Предоставляет готовую интеграцию AdminLTE в Laravel)