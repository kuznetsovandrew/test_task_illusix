<?php

use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $allIngredients = \App\Models\Ingredient::all();
        $quantityTypes = collect(['шт', 'кг', 'гр']);
        factory(\App\Models\Recipe::class, 10)
            ->create()
            ->each(function (\App\Models\Recipe $recipe) use ($allIngredients, $quantityTypes) {
                $ingredients = $allIngredients->random(random_int(2, 5));
                foreach ($ingredients as $ingredient) {
                    $quantityType = $quantityTypes->random(1)->first();
                    $recipe->ingredients()->create([
                        'ingredient_id' => $ingredient->id,
                        'quantity' => random_int(1, 50) . ' ' . $quantityType,
                    ]);
                }
            });
    }
}
