<?php

use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'Колбаса',
            'Редис',
            'Морковь',
            'Майонез',
            'Лук',
            'Масло',
        ];

        foreach ($data as $ingredientName) {
            \App\Models\Ingredient::create([
                'name' => $ingredientName
            ]);
        }
    }
}
