@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Просмотр рецепта</h1>
@stop

@section('content')
    <div class="col-md-6">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Просмотр рецепта</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <h1>{{$recipe->name}}</h1>
                    </div>
                </div>

                <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
                    <div class="col-sm-12">
                        <p>{{$recipe->description}}</p>
                    </div>
                </div>

                <div>
                    <table class="table table-bordered text-center">
                        <thead>
                        <tr>
                            <td>Название</td>
                            <td>Количество</td>
                        </tr>
                        </thead>
                        <tbody id="ingredients">
                        @foreach($recipe->ingredients as $recipeIngredient)
                            <tr>
                                <td>{{$recipeIngredient->name}}</td>
                                <td>
                                    <input data-ingrediant-id="{{$recipeIngredient->ingredient_id}}"
                                           class="input-quantity"
                                           type="text"
                                           name=""
                                           value="{{$recipeIngredient->quantity}}">
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <a href="{{route('recipes.index')}}" type="submit" class="btn btn-info pull-right">Назад к списку</a>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </div>

@stop

@section('js')
    <script>
        $('.input-quantity').on('change', function () {
            var quantity = $(this).val();
            var ingredientId = $(this).data('ingrediant-id');
            var recipeId = {{$recipe->id}};
            $.ajax({
                url: '{{route('recipes.updateRecipeIngredient')}}',
                data: {
                    ingredient_id: ingredientId,
                    recipe_id: recipeId,
                    quantity: quantity
                },
                success: function (result) {
                    if (result && result['status'] === 1) {
                        alert(result['text']);
                    }
                }
            });
        });
    </script>
@stop