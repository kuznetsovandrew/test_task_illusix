@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Создание рецепта</h1>
@stop

@section('content')
    <div class="col-md-6">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Создание рецепта</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{route('recipes.store')}}">
                @csrf
                <div class="box-body">
                    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                        <label for="input-name" class="col-sm-2 control-label">Название</label>

                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   name="name"
                                   value="{{old('name')}}"
                                   id="input-name"
                                   placeholder="Название ингредиента">
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group {{$errors->has('description') ? 'has-error' : ''}}">
                        <label for="input-description" class="col-sm-2 control-label">Описание</label>

                        <div class="col-sm-10">
                            <textarea class="form-control"
                                      name="description"
                                      id="input-description"
                                      placeholder="Название рецепта">{{old('description')}}</textarea>
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('description')}}</span>
                            @endif
                        </div>
                    </div>

                    <div>
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <td>Название</td>
                                <td>Количество</td>
                                <td>Действие</td>
                            </tr>
                            </thead>
                            <tbody id="ingredients">
                            </tbody>
                        </table>
                    </div>
                    <div>
                        <div class="col-sm-4">
                            <button class="btn btn-success" id="add-ingredient">Добавить</button>
                        </div>
                        <div class="col-sm-4"><span>Нет в списке?</span></div>
                        <div class="col-sm-4"><a class="mfp-popup" href="#create-ingredient">Создать новый
                                ингредиент</a></div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-info" href="{{route('recipes.index')}}">К списку</a>
                    <button type="submit" class="btn btn-success pull-right">Создать</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>

    <div class="hidden">
        <form id="create-ingredient" class="popup">
            @csrf
            <div class="form-group">
                <label for="ingredient-name" class="col-sm-2 control-label">Название</label>
                <div class="col-sm-10">
                    <input id="ingredient-name"
                           class="form-control"
                           type="text"
                           name="name"
                           placeholder="Название ингредиента">
                </div>
            </div>
            <button type="submit" class="btn btn-success">Создать</button>
        </form>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">
    <style>
        .popup {
            padding: 60px 30px 30px;
            background: #fff;
            position: relative;
            max-width: 500px;
            margin: auto;
        }
    </style>
@stop

@section('js')
    <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.mfp-popup').magnificPopup({
                type: 'inline',
                preloader: false,
                focus: '#name',

                // When elemened is focused, some mobile browsers in some cases zoom in
                // It looks not nice, so we disable it:
                callbacks: {
                    beforeOpen: function () {
                        if ($(window).width() < 700) {
                            this.st.focus = false;
                        } else {
                            this.st.focus = '#name';
                        }
                    }
                }
            });
        });

        var ingredientsTable = {
            ingredients: @json($ingredients),
            recipeIngredients: [],
            updateRecipeIngredients: function () {
                this.recipeIngredients = [];
                var rows = $('#ingredients tr');
                for (var i = 0; i < rows.length; i++ ) {
                    var row = rows[i];
                    var ingredientId = $(row).find('select').val();
                    var quantity = $(row).find('input').val();
                    this.recipeIngredients.push({
                        ingredientId: ingredientId,
                        quantity: quantity
                    });
                }
            },
            updateRows: function () {
                $('#ingredients').html('');
                for (var i = 0; i < this.recipeIngredients.length; i++) {
                    var recipeIngredient = this.recipeIngredients[i];
                    var template = $(this.rowTemplate).clone();
                    template.find('input').attr('name', 'ingredients[' + i +'][quantity]').val(recipeIngredient.quantity);
                    template.find('select').attr('name', 'ingredients[' + i +'][ingredient_id]');
                    var options = '';
                    for (var b = 0; b < this.ingredients.length; b++) {
                        var ingredient = this.ingredients[b];
                        var isSelected = parseInt(ingredient.id) == parseInt(recipeIngredient.ingredientId);
                        options += '<option value="' + ingredient.id + '" '
                            + (isSelected ? 'selected' : '') + '>'
                            + ingredient.name + '</option>'
                    }
                    template.find('select').html(options);
                    $('#ingredients').append(template);
                }

            },
            addRow: function () {
                $('#ingredients').append(this.rowTemplate);
            },
            rowTemplate: '<tr>\n' +
                '             <td>\n' +
                '                 <select name="">\n' +
                '                 </select>\n' +
                '             </td>\n' +
                '             <td>\n' +
                '                 <input type="text" name="">\n' +
                '             </td>\n' +
                '             <td>\n' +
                '                 <button class="btn btn-danger delete-ingredient">Удалить</button>\n' +
                '             </td>\n' +
                '         </tr>'
        };

        ingredientsTable.updateRecipeIngredients();
        ingredientsTable.updateRows();

        $('#add-ingredient').on('click', function (e) {
            e.preventDefault();
            ingredientsTable.addRow();
            ingredientsTable.updateRecipeIngredients();
            ingredientsTable.updateRows();
        });

        $(document).on('click', '.delete-ingredient', function (e) {
            e.preventDefault();
            $(this).closest('tr').remove();
            ingredientsTable.updateRecipeIngredients();
            ingredientsTable.updateRows();
        });

        $('#create-ingredient').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                'url': '{{route('ingredients.store')}}',
                'data': $(this).serialize(),
                'type': 'post',
                'success': function (result) {
                    if (result && result['status'] === 1) {
                        ingredientsTable.ingredients = result['ingredients'];
                        ingredientsTable.updateRows();
                        alert('Ингредиент создан');
                    }
                }
            });
        });

    </script>
@stop