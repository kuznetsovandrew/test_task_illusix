@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Создание игредиента</h1>
@stop

@section('content')
    <div class="col-md-6">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Создание игредиента</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="POST" action="{{route('ingredients.store')}}">
                @csrf
                <div class="box-body">
                    <div class="form-group {{$errors->has('name') ? 'has-error' : ''}}">
                        <label for="input-name" class="col-sm-2 control-label">Название</label>

                        <div class="col-sm-10">
                            <input type="text"
                                   class="form-control"
                                   name="name"
                                   value="{{old('name')}}"
                                   id="input-name"
                                   placeholder="Название ингредиента">
                            @if($errors->has('name'))
                                <span class="help-block">{{$errors->first('name')}}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a class="btn btn-info" href="{{route('ingredients.index')}}">К списку</a>
                    <button type="submit" class="btn btn-success pull-right">Создать</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
@stop