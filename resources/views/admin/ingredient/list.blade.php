@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Ингредиенты</h1>
@stop

@section('content')
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Ингредиенты</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                    <div class="row">
                        <div class="col-sm-6"></div>
                        <div class="col-sm-6"><a href="{{route('ingredients.create')}}" class="pull-right"><button class="btn btn-success">Создать</button></a></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid"
                                   aria-describedby="example2_info">
                                <thead>
                                <tr role="row">
                                    <th>Название</th>
                                    <th>Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($ingredients as $ingredient)
                                <tr role="row" class="odd">
                                    <td>{{$ingredient->name}}</td>
                                    <td>
                                        <a href="{{route('ingredients.edit', $ingredient->id)}}"><button class="btn btn-info"><i class="fa fa-edit"></i></button></a>
                                        <form action="{{route('ingredients.destroy', $ingredient->id)}}" method="post" class="inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr role="row">
                                    <th>Название</th>
                                    <th>Действия</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-5">
{{--                            <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1--}}
{{--                                to 10 of 57 entries--}}
{{--                            </div>--}}
                        </div>
                        <div class="col-sm-7">
                            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                                {{$ingredients->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
@stop