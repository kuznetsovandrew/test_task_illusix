<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\IngredientRequest;
use App\Models\Ingredient;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $ingredients = Ingredient::paginate(15);
        return view('admin.ingredient.list', [
            'ingredients' => $ingredients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.ingredient.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IngredientRequest $request
     * @return Response
     */
    public function store(IngredientRequest $request)
    {
        Ingredient::create($request->all());

        if ($request->ajax()) {
            $ingredients = Ingredient::get(['id', 'name'])->toArray();
            return response()->json(['status' => 1, 'ingredients' => $ingredients]);
        }

        return redirect()->route('ingredients.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Ingredient $ingredient
     * @return Response
     */
    public function edit(Ingredient $ingredient)
    {
        return view('admin.ingredient.edit', [
            'ingredient' => $ingredient
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param IngredientRequest $request
     * @param Ingredient $ingredient
     * @return Response
     */
    public function update(IngredientRequest $request, Ingredient $ingredient)
    {
        $ingredient->update($request->all());
        return redirect()->route('ingredients.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ingredient $ingredient
     * @return Response
     * @throws \Exception
     */
    public function destroy(Ingredient $ingredient)
    {
        $ingredient->delete();
        return redirect()->route('ingredients.index');
    }
}
