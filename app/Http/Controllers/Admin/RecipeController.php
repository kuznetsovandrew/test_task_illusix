<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\RecipeRequest;
use App\Models\Ingredient;
use App\Models\Recipe;
use App\Http\Controllers\Controller;
use App\Models\RecipeIngredient;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $recipes = Recipe::paginate();
        return view('admin.recipe.list', [
            'recipes' => $recipes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $ingredients = Ingredient::all();
        return view('admin.recipe.create', [
            'ingredients' => $ingredients
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RecipeRequest $request
     * @return Response
     */
    public function store(RecipeRequest $request)
    {
        $recipe = Recipe::create($request->all());
        if ($request->has('ingredients')) {
            foreach ($request->input('ingredients') as $item) {
                $recipe->ingredients()->create($item);
            }
        }

        return redirect()->route('recipes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Recipe $recipe
     * @return Response
     */
    public function show(Recipe $recipe)
    {
        $recipe->load('ingredients.ingredient');
        return view('admin.recipe.show', [
            'recipe' => $recipe
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Recipe $recipe
     * @return Response
     */
    public function edit(Recipe $recipe)
    {
        $recipe->load('ingredients.ingredient');
        $ingredients = Ingredient::all();
        return view('admin.recipe.edit', [
            'recipe' => $recipe,
            'ingredients' => $ingredients
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RecipeRequest $request
     * @param Recipe $recipe
     * @return Response
     */
    public function update(RecipeRequest $request, Recipe $recipe)
    {
        $recipe->update($request->all());
        $recipe->ingredients()->delete();
        if ($request->has('ingredients')) {
            foreach ($request->input('ingredients') as $item) {
                $recipe->ingredients()->create($item);
            }
        }
        return redirect()->route('recipes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recipe $recipe
     * @return Response
     * @throws \Exception
     */
    public function destroy(Recipe $recipe)
    {
        $recipe->delete();
        return redirect()->route('recipes.index');
    }

    public function updateRecipeIngredient(Request $request)
    {
        $this->validate($request, [
            'ingredient_id' => 'required',
            'recipe_id' => 'required'
        ]);

        $recipeIngredient = RecipeIngredient::where('ingredient_id', '=', $request->input('ingredient_id'))
            ->where('recipe_id', '=', $request->input('recipe_id'))
            ->first();

        if (isset($recipeIngredient)) {
            $recipeIngredient->quantity = $request->input('quantity');
            $recipeIngredient->save();
            return response()->json(['status' => 1, 'text' => 'Количество обновлено']);
        }
    }
}
