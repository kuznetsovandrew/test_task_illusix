<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RecipeIngredient
 *
 * @property-read \App\Models\Recipe $recipe
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient query()
 * @mixin \Eloquent
 * @property int $id
 * @property int $recipe_id
 * @property string $ingredient
 * @property string|null $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereIngredient($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereRecipeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereUpdatedAt($value)
 * @property int $ingredient_id
 * @property-read mixed $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RecipeIngredient whereIngredientId($value)
 */
class RecipeIngredient extends Model
{
    protected $fillable = [
        'recipe_id',
        'ingredient_id',
        'quantity',
    ];

    public function recipe()
    {
        return $this->belongsTo(Recipe::class);
    }

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }

    public function getNameAttribute()
    {
        return $this->ingredient->name ?? '';
    }
}
